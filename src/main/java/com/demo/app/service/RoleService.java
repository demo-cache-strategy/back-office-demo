package com.demo.app.service;

import com.demo.app.service.dto.RoleDto;

public interface RoleService extends Service<RoleDto> {

	RoleDto findByName(final String name);

}
