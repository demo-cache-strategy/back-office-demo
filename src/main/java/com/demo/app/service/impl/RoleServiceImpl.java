package com.demo.app.service.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.demo.app.feing.cache.CacheServiceFeign;
import com.demo.app.util.CommonConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.app.exception.BadRequestException;
import com.demo.app.exception.NotFoundException;
import com.demo.app.repository.RoleRepository;
import com.demo.app.repository.UserRepository;
import com.demo.app.repository.entity.Role;
import com.demo.app.repository.entity.User;
import com.demo.app.service.RoleService;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import com.demo.app.service.mapper.ServiceMapper;

@Service("roleService")
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

	private final ServiceMapper<RoleDto, Role> roleServiceMapper;
	private final CacheServiceFeign cacheServiceFeign;

	@Override
	public List<RoleDto> findAll() {
		return cacheServiceFeign.getAllRoles();
	}

	@Transactional(readOnly = true)
	@Override
	public RoleDto findById(final Integer id) {
		if (null == id) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		return cacheServiceFeign.getRoleById(RoleDto.builder().id(id).build());
	}

	@Transactional(readOnly = true)
	@Override
	public RoleDto findByName(final String name) {
		if (null == name) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		return cacheServiceFeign.getRoleByName(RoleDto.builder().name(name).build());
	}

	@Transactional
	@Override
	public RoleDto save(final RoleDto roleDto) {
		if (null == roleDto) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		return cacheServiceFeign.addRole(roleDto);

	}

	@Transactional
	@Override
	public RoleDto update(final RoleDto roleDto) {
		if (null == roleDto || null == roleDto.getId()) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		return cacheServiceFeign.addRole(roleDto);
	}

	@Transactional
	@Override
	public RoleDto delete(final RoleDto roleDto) {
		if (null == roleDto || null == roleDto.getId()) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		return cacheServiceFeign.deleteRole(roleDto);
	}

}
