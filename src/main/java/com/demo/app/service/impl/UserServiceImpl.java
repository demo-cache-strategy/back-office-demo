package com.demo.app.service.impl;

import com.demo.app.exception.BadRequestException;
import com.demo.app.exception.NotFoundException;
import com.demo.app.feing.cache.CacheServiceFeign;
import com.demo.app.repository.RoleRepository;
import com.demo.app.repository.UserRepository;
import com.demo.app.repository.entity.Role;
import com.demo.app.repository.entity.User;
import com.demo.app.service.UserService;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import com.demo.app.service.mapper.ServiceMapper;
import com.demo.app.util.CommonConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	private final ServiceMapper<UserDto, User> userServiceMapper;
	private final RoleRepository roleRepository;
	private final CacheServiceFeign cacheServiceFeign;

	@Transactional(readOnly = true)
	@Override
	public List<UserDto> findAll() {
		List<UserDto> userDtoList = cacheServiceFeign.getAllUsers();

		if (!userDtoList.isEmpty()){
			return userDtoList;
		}

		userDtoList = this.getUsersFromRepository();

		cacheServiceFeign.addUser(userDtoList);

		return userDtoList;
	}

	private List<UserDto> getUsersFromRepository() {
		return userServiceMapper.mapToDtoList(userRepository.findAll());
	}

	@Transactional(readOnly = true)
	@Override
	public UserDto findById(final Integer id) {
		if (null == id) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		try{
			return cacheServiceFeign.getUserById(UserDto.builder().id(id).build());
		} catch (NotFoundException nfe) {
			final User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("404", CommonConstants.USER_NOT_FOUND));

			try{
				cacheServiceFeign.addUser(userServiceMapper.mapToDto(user));
			} catch (Exception e) {
				//TODO manjear fallo en insert sobre cache
			}

			return userServiceMapper.mapToDto(user);
		}
	}

	@Transactional(readOnly = true)
	@Override
	public UserDto findByName(final String name) {
		if (null == name) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		try{
			return cacheServiceFeign.getUserByName(UserDto.builder().name(name).build());
		} catch (NotFoundException nfe) {
			final User user = userRepository.findByName(name).orElseThrow(() -> new NotFoundException("404", CommonConstants.USER_NOT_FOUND));

			try{
				cacheServiceFeign.addUser(userServiceMapper.mapToDto(user));
			} catch (Exception e) {
				//TODO manjear fallo en insert sobre cache
			}

			return userServiceMapper.mapToDto(user);
		}
	}

	@Transactional(readOnly = true)
	@Override
	public UserDto findByUsername(final String username) {
		if (null == username) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		try{
			return cacheServiceFeign.getUserByUsername(UserDto.builder().username(username).build());
		} catch (NotFoundException nfe) {
			final User user = userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException("404", CommonConstants.USER_NOT_FOUND));

			try{
				cacheServiceFeign.addUser(userServiceMapper.mapToDto(user));
			} catch (Exception e) {
				//TODO manjear fallo en insert sobre cache
			}

			return userServiceMapper.mapToDto(user);
		}
	}

	@Transactional
	@Override
	public UserDto save(final UserDto userDto) {
		if (null == userDto) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		final User user = new User();
		user.setName(userDto.getName());
		user.setUsername(userDto.getUsername());
		user.setPassword(userDto.getPassword());
		user.setMail(userDto.getMail());
		user.setStatus(userDto.getStatus());

		final Set<Role> roles = getRoles(userDto.getRoles());

		user.setRoles(roles);

		return userServiceMapper.mapToDto(userRepository.save(user));
	}

	@Transactional
	@Override
	public UserDto update(final UserDto userDto) {
		if (null == userDto || null == userDto.getId()) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		User user = userRepository.findById(userDto.getId())
				.orElseThrow(() -> new NotFoundException("404", "User not exist"));

		final Set<Role> roles = getRoles(userDto.getRoles());

		user.setPassword(userDto.getPassword());
		user.setRoles(roles);

		user = userRepository.save(user);

		return userServiceMapper.mapToDto(user);
	}

	private final Set<Role> getRoles(final List<RoleDto> roles) {
		if (null == roles || roles.isEmpty()) {
			return Collections.emptySet();
		}

		final Set<Role> entityRoles = new HashSet<>();

		for (final RoleDto dtoRole : roles) {
			if (null != dtoRole) {
				final Role entityRole = roleRepository.findById(dtoRole.getId()).orElse(null);
				if (null != entityRole) {
					entityRoles.add(entityRole);
				}
			}
		}

		return entityRoles;
	}

	@Transactional
	@Override
	public UserDto delete(final UserDto userDto) {
		if (null == userDto || null == userDto.getId()) {
			throw new BadRequestException(CommonConstants.WRONG_PARAMETER_IN_REQUEST);
		}

		userRepository.deleteById(userDto.getId());
		return userDto;
	}

}
