package com.demo.app.service.mapper.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.demo.app.repository.entity.Role;
import com.demo.app.repository.entity.User;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import com.demo.app.service.mapper.ServiceMapper;

@Component
public class RoleServiceMapper implements ServiceMapper<RoleDto, Role> {

	@Override
	public Role mapToEntity(final RoleDto roleDto) {
		final Role role = new Role();

		if (null != roleDto) {
			if (null == roleDto.getId() || roleDto.getId() <= 0) {
				role.setId(null);
			} else {
				role.setId(roleDto.getId());
			}

			role.setName(roleDto.getName());

			role.setUsers(getUserList(roleDto));
		}

		return role;
	}

	private Set<User> getUserList(final RoleDto roleDto) {
		final Set<User> listUserDto = new HashSet<>();

		if (null != roleDto.getUsers() && !roleDto.getUsers().isEmpty()) {
			roleDto.getUsers().stream().forEach(userDto -> {
				final User user = new User();
				user.setId(userDto.getId());
				user.setName(userDto.getName());
				user.setUsername(userDto.getUsername());
				user.setPassword(userDto.getPassword());
				user.setMail(userDto.getMail());
				listUserDto.add(user);
			});
		}
		return listUserDto;
	}

	@Override
	public RoleDto mapToDto(final Role role) {
		final RoleDto roleDto = new RoleDto();

		if (null != role) {
			roleDto.setId(role.getId());
			roleDto.setName(role.getName());
			if (null != role.getUsers()) {
				roleDto.setUsers(getUserDtoList(role));
			}
		}
		return roleDto;
	}

	private List<UserDto> getUserDtoList(final Role role) {
		final List<UserDto> listUserDto = new ArrayList<>();

		if (null != role.getUsers()) {
			role.getUsers().stream().forEach(user -> {
				final UserDto userDto = new UserDto();
				userDto.setId(user.getId());
				userDto.setName(user.getName());
				userDto.setUsername(user.getUsername());
				userDto.setPassword(user.getPassword());
				userDto.setMail(user.getMail());
				listUserDto.add(userDto);
			});
		}
		return listUserDto;
	}

	@Override
	public Set<Role> mapToEntityList(final Collection<RoleDto> roleDtoList) {
		if (roleDtoList == null) {
			return Collections.emptySet();
		}

		return roleDtoList.stream().map(this::mapToEntity).collect(Collectors.toSet());
	}

	@Override
	public List<RoleDto> mapToDtoList(final Collection<Role> roles) {
		if (roles == null) {
			return Collections.emptyList();
		}

		return roles.stream().map(this::mapToDto).collect(Collectors.toList());
	}

}
