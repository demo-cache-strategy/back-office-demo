package com.demo.app.service.mapper.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.demo.app.repository.entity.Role;
import com.demo.app.repository.entity.User;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import com.demo.app.service.mapper.ServiceMapper;

@Component
public class UserServiceMapper implements ServiceMapper<UserDto, User> {

	@Override
	public User mapToEntity(final UserDto dto) {
		final User user = new User();

		if (null != dto) {
			if (null == dto.getId() || dto.getId() <= 0) {
				user.setId(null);
			} else {
				user.setId(dto.getId());
			}

			user.setName(dto.getName());
			user.setUsername(dto.getUsername());
			user.setPassword(dto.getPassword());
			user.setMail(dto.getMail());
			user.setStatus(dto.getStatus());
			user.setRoles(mapRolesEntity(dto.getRoles()));
		}

		return user;
	}

	@Override
	public UserDto mapToDto(final User user) {
		final UserDto dto = new UserDto();

		if (null != user) {
			dto.setId(user.getId());
			dto.setName(user.getName());
			dto.setUsername(user.getUsername());
			dto.setPassword(user.getPassword());
			dto.setMail(user.getMail());
			dto.setStatus(user.getStatus());
			dto.setRoles(mapRolesDto(user.getRoles()));
		}
		return dto;
	}

	private Set<Role> mapRolesEntity(final List<RoleDto> roles) {
		final Set<Role> roleList = new HashSet<>();

		if (null != roles && !roles.isEmpty()) {
			for (final RoleDto role : roles) {
				final Role roleEntity = new Role();
				roleEntity.setId(role.getId());
				roleEntity.setName(role.getName());
				roleList.add(roleEntity);
			}

		}
		return roleList;
	}

	private List<RoleDto> mapRolesDto(final Set<Role> roles) {
		final List<RoleDto> roleList = new ArrayList<>();

		if (null != roles && !roles.isEmpty()) {
			for (final Role role : roles) {
				final RoleDto roleEntity = new RoleDto();
				roleEntity.setId(role.getId());
				roleEntity.setName(role.getName());
				roleList.add(roleEntity);
			}

		}
		return roleList;
	}

	@Override
	public Set<User> mapToEntityList(final Collection<UserDto> dtoList) {
		if (dtoList == null) {
			return Collections.emptySet();
		}

		return dtoList.stream().map(this::mapToEntity).collect(Collectors.toSet());
	}

	@Override
	public List<UserDto> mapToDtoList(final Collection<User> users) {
		if (users == null) {
			return Collections.emptyList();
		}

		return users.stream().map(this::mapToDto).collect(Collectors.toList());
	}

}
