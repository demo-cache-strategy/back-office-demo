package com.demo.app.service.mapper;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface ServiceMapper<D, E> {

	E mapToEntity(D dto);

	D mapToDto(E entity);

	Set<E> mapToEntityList(final Collection<D> dtoList);

	List<D> mapToDtoList(final Collection<E> entityList);

}
