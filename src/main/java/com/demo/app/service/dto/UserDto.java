package com.demo.app.service.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {

	private static final long serialVersionUID = 323457144665707542L;

	private Integer id;

	private String name;

	private String username;

	private String password;

	private String mail;

	private String status;

	private List<RoleDto> roles;

}
