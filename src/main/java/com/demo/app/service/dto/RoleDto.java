package com.demo.app.service.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto implements Serializable {

	private static final long serialVersionUID = 1259527153533616144L;

	private Integer id;

	private String name;

	private List<UserDto> users;

}
