package com.demo.app.service;

import java.util.List;

public interface Service<D> {
	List<D> findAll();

	D findById(Integer id);

	D save(D dto);

	D update(D dto);

	D delete(D dto);

}
