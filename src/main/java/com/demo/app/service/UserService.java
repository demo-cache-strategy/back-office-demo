package com.demo.app.service;

import com.demo.app.service.dto.UserDto;

public interface UserService extends Service<UserDto> {

	UserDto findByName(String name);

	UserDto findByUsername(String username);

}
