package com.demo.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.app.repository.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	Optional<User> findByName(String name);

	Optional<User> findByUsername(String username);
}
