package com.demo.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class BackOfficeDemoApplication {

	public static void main(final String[] args) {
		SpringApplication.run(BackOfficeDemoApplication.class, args);
	}

}
