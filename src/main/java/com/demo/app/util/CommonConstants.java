package com.demo.app.util;

public class CommonConstants {

	// GENERIC STATUS
	public static final String SUCCESS = "Success";
	public static final String ERROR = "Error";
	public static final String OK = "Ok";
    public static final String WRONG_PARAMETER_IN_REQUEST = "Wrong parameter in request";
	public static final String ROLE_NOT_FOUND = "Role not found.";
	public static final String USER_NOT_FOUND = "User not found.";

	private CommonConstants() {
		throw new IllegalStateException("Utility Class");
	}

}
