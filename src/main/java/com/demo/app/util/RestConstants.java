package com.demo.app.util;

public class RestConstants {

	public static final String APPLICATION_NAME = "/back-office";

	public static final String API_VERSION_1 = "/v1";

	public static final String RESOURCE_USER = "/users";

	public static final String RESOURCE_ROLE = "/roles";

	public static final String RESOURCE_ID = "/{id}";

	public static final String RESOURCE_NAME = "/{name}";

	public static final String RESOURCE_USERNAME = "/{username}";

	private RestConstants() {
		throw new IllegalStateException("Utility Class");
	}

}
