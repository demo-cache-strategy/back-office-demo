package com.demo.app.util;

public class ExceptionConstants {

	// CODES
	public static final String CODE_INTERNAL_SERVER_ERROR = "SPRO-500";
	public static final String CODE_NOT_FOUND = "SPRO-404-1";
	public static final String CODE_BAD_REQUEST = "SPRO-400-1";

	// MESSAGES
	public static final String ERROR = "Error";
	public static final String BAD_REQUEST = "BAD REQUEST - The input was wrong";

	private ExceptionConstants() {
		throw new IllegalStateException("Utility Class");
	}

}
