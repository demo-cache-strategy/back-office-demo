package com.demo.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Locale;

@Configuration
public class LocaleConfig {

	@Bean
	public void localeDefaultSetter() {
		Locale.setDefault(new Locale("es", "ES"));
	}

}
