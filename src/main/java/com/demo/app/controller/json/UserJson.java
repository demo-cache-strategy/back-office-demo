package com.demo.app.controller.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserJson implements Serializable {

	private static final long serialVersionUID = 3072855977168641616L;

	private Integer id;
	private String name;
	private String username;
	private String password;
	private String status;
	private String mail;
	private List<Integer> roles;

}
