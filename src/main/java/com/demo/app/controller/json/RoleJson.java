package com.demo.app.controller.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleJson implements Serializable {

	private static final long serialVersionUID = 7090745556384816652L;

	private Integer id;
	private String name;
	private List<Integer> users;

}
