package com.demo.app.controller.json;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataJson<T extends Serializable> implements Serializable {

	private static final long serialVersionUID = 5363063676846907212L;

	private String status;

	private String code;

	private String message;

	private T data;

	public DataJson(final String status, final String code, final String message) {
		this.status = status;
		this.code = code;
		this.message = message;
	}

}
