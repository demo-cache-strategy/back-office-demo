package com.demo.app.controller.mapper.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.demo.app.controller.json.RoleJson;
import com.demo.app.controller.mapper.ControllerMapper;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;

@Component
public class RoleControllerMapper implements ControllerMapper<RoleDto, RoleJson> {

	@Override
	public RoleJson mapToJson(final RoleDto dto) {
		if (null == dto) {
			return null;
		}

		final RoleJson json = new RoleJson();
		json.setId(dto.getId());
		json.setName(dto.getName());
		if (null != dto.getUsers()) {
			json.setUsers(dto.getUsers().stream().map(UserDto::getId).collect(Collectors.toList()));
		}

		return json;
	}

	@Override
	public RoleDto mapToDto(final RoleJson json) {
		if (null == json) {
			return null;
		}

		final RoleDto dto = new RoleDto();
		dto.setId(json.getId());
		dto.setName(json.getName());
		if (null != json.getUsers() && !json.getUsers().isEmpty()) {
			dto.setUsers(new ArrayList<UserDto>());
			json.getUsers().stream().forEach(userId -> {
				final UserDto userDto = new UserDto();
				userDto.setId(userId);
				dto.getUsers().add(userDto);
			});
		}

		return dto;
	}

	@Override
	public List<RoleJson> mapToJsonList(final Collection<RoleDto> dtoList) {
		return dtoList.stream().map(this::mapToJson).collect(Collectors.toList());
	}

	@Override
	public List<RoleDto> mapToDtoList(final Collection<RoleJson> jsonList) {
		return jsonList.stream().map(this::mapToDto).collect(Collectors.toList());
	}

}
