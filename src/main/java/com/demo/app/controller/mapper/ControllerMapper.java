package com.demo.app.controller.mapper;

import java.util.Collection;
import java.util.List;

public interface ControllerMapper<D, J> {

	J mapToJson(D dto);

	D mapToDto(J json);

	List<D> mapToDtoList(Collection<J> roleJsonList);

	List<J> mapToJsonList(Collection<D> usersDto);

}
