package com.demo.app.controller.mapper.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.demo.app.controller.json.UserJson;
import com.demo.app.controller.mapper.ControllerMapper;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;

@Component
public class UserControllerMapper implements ControllerMapper<UserDto, UserJson> {

	@Override
	public UserJson mapToJson(final UserDto dto) {
		if (null == dto) {
			return null;
		}

		final UserJson json = new UserJson();

		json.setId(dto.getId());
		json.setName(dto.getName());
		json.setUsername(dto.getUsername());
		json.setPassword(dto.getPassword());
		json.setMail(dto.getMail());
		json.setStatus(dto.getStatus());
		if (null != dto.getRoles()) {
			json.setRoles(dto.getRoles().stream().map(RoleDto::getId).collect(Collectors.toList()));
		}

		return json;
	}

	@Override
	public UserDto mapToDto(final UserJson json) {
		if (null == json) {
			return null;
		}

		final UserDto dto = new UserDto();

		dto.setId(json.getId());
		dto.setName(json.getName());
		dto.setUsername(json.getUsername());
		dto.setPassword(json.getPassword());
		dto.setMail(json.getMail());
		dto.setStatus(json.getStatus());
		if (null != json.getRoles() && !json.getRoles().isEmpty()) {
			dto.setRoles(new ArrayList<RoleDto>());
			json.getRoles().stream().forEach(roleId -> {
				final RoleDto roleDto = new RoleDto();
				roleDto.setId(roleId);
				dto.getRoles().add(roleDto);
			});
		}

		return dto;
	}

	@Override
	public List<UserJson> mapToJsonList(final Collection<UserDto> dtoList) {
		return dtoList.stream().map(this::mapToJson).collect(Collectors.toList());
	}

	@Override
	public List<UserDto> mapToDtoList(final Collection<UserJson> jsonList) {
		return jsonList.stream().map(this::mapToDto).collect(Collectors.toList());
	}

}
