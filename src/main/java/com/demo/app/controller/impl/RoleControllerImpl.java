package com.demo.app.controller.impl;

import com.demo.app.controller.RoleController;
import com.demo.app.controller.json.DataJson;
import com.demo.app.controller.json.RoleJson;
import com.demo.app.controller.mapper.ControllerMapper;
import com.demo.app.service.RoleService;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.util.CommonConstants;
import com.demo.app.util.RestConstants;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@Api(tags = "Roles")
@CrossOrigin(origins = "*", maxAge = 2000)
@RestController("roleController")
@RequestMapping(value = RestConstants.APPLICATION_NAME + RestConstants.API_VERSION_1 + RestConstants.RESOURCE_ROLE)
@RequiredArgsConstructor
public class RoleControllerImpl implements RoleController {

	private final RoleService roleService;
	private final ControllerMapper<RoleDto, RoleJson> controllerMapper;

	@ApiOperation(value = "Return all roles")
	@ApiResponses({ @ApiResponse(code = 200, message = "Roles found", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class),
			@ApiResponse(code = 404, message = "Roles not found", response = DataJson.class) })
	@GetMapping
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<ArrayList<RoleJson>> findAllRoles() {
		final DataJson<ArrayList<RoleJson>> dataJson = new DataJson<>();
		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);
		dataJson.setData((ArrayList<RoleJson>) controllerMapper.mapToJsonList((roleService.findAll())));
		return dataJson;
	}

	@ApiOperation(value = "Return a role by Id")
	@ApiParam(value = "Id Role")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role found", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class),
			@ApiResponse(code = 404, message = "Role not found", response = DataJson.class) })
	@GetMapping(RestConstants.RESOURCE_ID)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<RoleJson> findRoleById(@PathVariable(value = "id") final Integer id) {
		final DataJson<RoleJson> dataJson = new DataJson<>();

		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		dataJson.setData(controllerMapper.mapToJson((roleService.findById(id))));
		return dataJson;
	}

	@ApiOperation(value = "Return a role by NAME")
	@ApiParam(value = "Name Role")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role found", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class),
			@ApiResponse(code = 404, message = "Role not found", response = DataJson.class) })
	@GetMapping("name" + RestConstants.RESOURCE_NAME)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<RoleJson> findRoleByName(@PathVariable(value = "name") final String name) {
		final DataJson<RoleJson> dataJson = new DataJson<>();

		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		dataJson.setData(controllerMapper.mapToJson(roleService.findByName(name)));
		return dataJson;
	}

	@ApiOperation(value = "Create a new role")
	@ApiParam(value = "Role information")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role added", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class) })
	@PostMapping(produces = "application/json", consumes = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public DataJson<RoleJson> addRole(@Valid @RequestBody final RoleJson roleJson) {
		final DataJson<RoleJson> dataJson = new DataJson<>();
		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		final RoleDto roleDto = controllerMapper.mapToDto(roleJson);

		dataJson.setData(controllerMapper.mapToJson(roleService.save(roleDto)));
		return dataJson;
	}

	@ApiOperation(value = "Update a role")
	@ApiParam(value = "Role information")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role updated", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class) })
	@PutMapping(produces = "application/json", consumes = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public DataJson<RoleJson> updateRole(@Valid @RequestBody final RoleJson roleJson) {
		final DataJson<RoleJson> dataJson = new DataJson<>();
		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		final RoleDto roleDto = controllerMapper.mapToDto(roleJson);

		dataJson.setData(controllerMapper.mapToJson(roleService.update(roleDto)));
		return dataJson;
	}

	@ApiOperation(value = "Delete a role")
	@ApiParam(value = "Id Role")
	@ApiResponses({ @ApiResponse(code = 200, message = "Role deleted", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class) })
	@DeleteMapping(RestConstants.RESOURCE_ID)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<RoleJson> deleteRole(@PathVariable(value = "id") final Integer id) {
		final DataJson<RoleJson> dataJson = new DataJson<>();
		final RoleDto roleDto = new RoleDto();

		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		roleDto.setId(id);

		dataJson.setData(controllerMapper.mapToJson(roleService.delete(roleDto)));

		return dataJson;
	}

}
