package com.demo.app.controller.impl;

import com.demo.app.controller.UserController;
import com.demo.app.controller.json.DataJson;
import com.demo.app.controller.json.UserJson;
import com.demo.app.controller.mapper.ControllerMapper;
import com.demo.app.exception.DemoException;
import com.demo.app.service.UserService;
import com.demo.app.service.dto.UserDto;
import com.demo.app.util.CommonConstants;
import com.demo.app.util.RestConstants;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@Api(tags = "Users")
@CrossOrigin(origins = "*", maxAge = 2000)
@RestController("userController")
@RequestMapping(value = RestConstants.APPLICATION_NAME + RestConstants.API_VERSION_1 + RestConstants.RESOURCE_USER)
@RequiredArgsConstructor
public class UserControllerImpl implements UserController {

	private final UserService userService;
	private final ControllerMapper<UserDto, UserJson> controllerMapper;

	@ApiOperation(value = "Return all users")
	@ApiResponses({ @ApiResponse(code = 200, message = "Users found", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DemoException.class),
			@ApiResponse(code = 404, message = "Users not found", response = DemoException.class) })
	@GetMapping
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<ArrayList<UserJson>> findAllUsers() {
		final DataJson<ArrayList<UserJson>> dataJson = new DataJson<>();
		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		dataJson.setData((ArrayList<UserJson>) controllerMapper.mapToJsonList((userService.findAll())));

		return dataJson;
	}

	@ApiOperation(value = "Return an user by Id")
	@ApiParam(value = "Id User")
	@ApiResponses({ @ApiResponse(code = 200, message = "User found", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class),
			@ApiResponse(code = 404, message = "User not found", response = DataJson.class) })
	@GetMapping(RestConstants.RESOURCE_ID)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<UserJson> findUserById(@PathVariable(value = "id") final Integer id) {
		final DataJson<UserJson> dataJson = new DataJson<>();
		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		dataJson.setData(controllerMapper.mapToJson(userService.findById(id)));
		return dataJson;
	}

	@ApiOperation(value = "Return an user by NAME")
	@ApiParam(value = "Name User")
	@ApiResponses({ @ApiResponse(code = 200, message = "User found", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class),
			@ApiResponse(code = 404, message = "User not found", response = DataJson.class) })
	@GetMapping("/name/" + RestConstants.RESOURCE_NAME)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<UserJson> findUserByName(@PathVariable(value = "name") final String name) {
		final DataJson<UserJson> dataJson = new DataJson<>();

		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		dataJson.setData(controllerMapper.mapToJson(userService.findByName(name)));

		return dataJson;
	}

	@ApiOperation(value = "Return an user by USERNAME")
	@ApiParam(value = "Username User")
	@ApiResponses({ @ApiResponse(code = 200, message = "User found", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class),
			@ApiResponse(code = 404, message = "User not found", response = DataJson.class) })
	@GetMapping("/username/" + RestConstants.RESOURCE_USERNAME)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<UserJson> findUserByUsername(@PathVariable(value = "username") final String username) {
		final DataJson<UserJson> dataJson = new DataJson<>();

		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		dataJson.setData(controllerMapper.mapToJson(userService.findByUsername(username)));

		return dataJson;
	}

	@ApiOperation(value = "Create an new user")
	@ApiParam(value = "User information")
	@ApiResponses({ @ApiResponse(code = 201, message = "User added", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class) })
	@PostMapping(produces = "application/json", consumes = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public DataJson<UserJson> addUser(@Valid @RequestBody final UserJson userJson) {
		final DataJson<UserJson> dataJson = new DataJson<>();
		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		final UserDto userDto = controllerMapper.mapToDto(userJson);

		dataJson.setData(controllerMapper.mapToJson(userService.save(userDto)));

		return dataJson;
	}

	@ApiOperation(value = "Update an user")
	@ApiParam(value = "User information")
	@ApiResponses({ @ApiResponse(code = 200, message = "User updated", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class) })
	@PutMapping(produces = "application/json", consumes = "application/json")
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public DataJson<UserJson> updateUser(@Valid @RequestBody final UserJson userJson) {
		final DataJson<UserJson> dataJson = new DataJson<>();
		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		final UserDto userDto = controllerMapper.mapToDto(userJson);

		dataJson.setData(controllerMapper.mapToJson(userService.update(userDto)));

		return dataJson;
	}

	@ApiOperation(value = "Delete an user")
	@ApiParam(value = "Id User")
	@ApiResponses({ @ApiResponse(code = 200, message = "User deleted", response = DataJson.class),
			@ApiResponse(code = 400, message = "General service error", response = DataJson.class) })
	@DeleteMapping(RestConstants.RESOURCE_ID)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@Override
	public DataJson<UserJson> deleteUser(@PathVariable(value = "id") final Integer id) {
		final DataJson<UserJson> dataJson = new DataJson<>();
		final UserDto userDto = new UserDto();

		dataJson.setStatus(CommonConstants.SUCCESS);
		dataJson.setCode(String.valueOf(HttpStatus.OK.value()));
		dataJson.setMessage(CommonConstants.OK);

		userDto.setId(id);

		dataJson.setData(controllerMapper.mapToJson(userService.delete(userDto)));
		return dataJson;
	}

}
