package com.demo.app.controller.impl;

import com.demo.app.controller.SwaggerController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class SwaggerControllerImpl implements SwaggerController {

	@Override
	@GetMapping({ "/", "/swagger" })
	public String swaggerHome() {
		return "redirect:swagger-ui.html";
	}
}
