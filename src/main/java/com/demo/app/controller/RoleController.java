package com.demo.app.controller;

import java.util.ArrayList;

import com.demo.app.controller.json.DataJson;
import com.demo.app.controller.json.RoleJson;

public interface RoleController {
	DataJson<ArrayList<RoleJson>> findAllRoles();

	DataJson<RoleJson> findRoleById(Integer id);

	DataJson<RoleJson> findRoleByName(String name);

	DataJson<RoleJson> addRole(RoleJson json);

	DataJson<RoleJson> updateRole(RoleJson json);

	DataJson<RoleJson> deleteRole(Integer id);

}
