package com.demo.app.controller.handler;

import com.demo.app.controller.json.DataJson;
import com.demo.app.exception.DemoException;
import com.demo.app.util.ExceptionConstants;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
@SuppressWarnings({ "rawtypes", "unchecked" })
public class DemoExceptionHandler {

	@ExceptionHandler({ Exception.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public DataJson unhandledErrors(final HttpServletRequest request, final Exception ex) {
		return new DataJson(ExceptionConstants.ERROR, ExceptionConstants.CODE_INTERNAL_SERVER_ERROR, ex.getMessage());
	}

	@ExceptionHandler({ DemoException.class })
	@ResponseBody
	public DataJson handleException(final HttpServletRequest request, final HttpServletResponse response,
			final DemoException ex) {
		response.setStatus(ex.getResponseCode());
		return new DataJson(ExceptionConstants.ERROR, ex.getCode(), ex.getMessage(), ex.getErrorList());
	}

}
