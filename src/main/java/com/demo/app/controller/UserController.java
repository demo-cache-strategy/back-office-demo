package com.demo.app.controller;

import com.demo.app.controller.json.DataJson;
import com.demo.app.controller.json.UserJson;
import com.demo.app.util.RestConstants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;

public interface UserController {
	DataJson<ArrayList<UserJson>> findAllUsers();

	DataJson<UserJson> findUserById(Integer id);

	DataJson<UserJson> findUserByName(String name);

    DataJson<UserJson> findUserByUsername(String username);

    DataJson<UserJson> addUser(UserJson json);

	DataJson<UserJson> updateUser(UserJson json);

	DataJson<UserJson> deleteUser(Integer id);

}
