package com.demo.app.feing.cache.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CacheResponse<T extends Serializable> implements Serializable {

    private static final long serialVersionUID = 7894512676846907212L;

    private String status;

    private String code;

    private String message;

    private T data;

    public CacheResponse(final String status, final String code, final String message) {
        this.status = status;
        this.code = code;
        this.message = message;
    }

}
