package com.demo.app.feing.cache.constants;

public class FeignConstants {

    //Especific Resources
    public static final String RESOURCE_USERS = "/users";
    public static final String RESOURCE_ROLES = "/roles";

    private FeignConstants() {
        throw new IllegalStateException("Utility Class");
    }

}
