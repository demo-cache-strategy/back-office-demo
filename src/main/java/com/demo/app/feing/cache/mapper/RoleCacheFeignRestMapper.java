package com.demo.app.feing.cache.mapper;

import com.demo.app.controller.json.RoleJson;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class RoleCacheFeignRestMapper {

    public RoleJson mapToRest(RoleDto dto) {
        if (null == dto) {
            return null;
        }

        final RoleJson json = new RoleJson();

        json.setId(dto.getId());
        json.setName(dto.getName());
        if (null != dto.getUsers()) {
            json.setUsers(dto.getUsers().stream().map(UserDto::getId).collect(Collectors.toList()));
        }

        return json;
    }

    public RoleDto mapToDto(RoleJson json) {
        if (null == json) {
            return null;
        }

        final RoleDto dto = new RoleDto();

        dto.setId(json.getId());
        dto.setName(json.getName());
        if (null != json.getUsers() && !json.getUsers().isEmpty()) {
            dto.setUsers(json.getUsers().stream().map(userId -> UserDto.builder().id(userId).build()).collect(Collectors.toList()));
        }

        return dto;
    }

    public List<RoleJson> mapToJsonList(final Collection<RoleDto> dtoList) {
        return dtoList.stream().map(this::mapToRest).collect(Collectors.toList());
    }

    public List<RoleDto> mapToDtoList(final Collection<RoleJson> jsonList) {
        return jsonList.stream().map(this::mapToDto).collect(Collectors.toList());
    }
}
