package com.demo.app.feing.cache;

import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;

import java.util.List;

public interface CacheServiceFeign {

    List<UserDto> getAllUsers();
    UserDto getUserById(UserDto userDto);
    UserDto getUserByName(UserDto userDto);
    UserDto getUserByUsername(UserDto userDto);
    UserDto addUser(UserDto userDto);
    List<UserDto> addUser(List<UserDto> userDtoList);
    List<RoleDto> getAllRoles();
    RoleDto getRoleById(RoleDto roleDto);
    RoleDto getRoleByName(RoleDto roleDto);
    RoleDto addRole(RoleDto roleDto);
    List<RoleDto> addRole(List<RoleDto> roleDtoList);
    RoleDto deleteRole(RoleDto roleDto);

}
