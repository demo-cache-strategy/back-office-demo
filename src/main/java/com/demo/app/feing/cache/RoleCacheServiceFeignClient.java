package com.demo.app.feing.cache;

import com.demo.app.controller.json.RoleJson;
import com.demo.app.feing.cache.constants.FeignConstants;
import com.demo.app.feing.cache.rest.CacheResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "cache-api", url = "${cache.server.url}")
public interface RoleCacheServiceFeignClient {

    @GetMapping(value = FeignConstants.RESOURCE_ROLES, produces = MediaType.APPLICATION_JSON_VALUE)
    CacheResponse<RoleJson[]> findAllRoles();

    @GetMapping(value = FeignConstants.RESOURCE_ROLES + "/{id}")
    CacheResponse<RoleJson> findById(@PathVariable(value = "id") final Integer id);

    @GetMapping(value = FeignConstants.RESOURCE_ROLES + "/name/{name}")
    CacheResponse<RoleJson> findByName(@PathVariable(value = "name") final String name);

    @PostMapping(value = FeignConstants.RESOURCE_ROLES, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    CacheResponse<RoleJson> addRole(@RequestBody RoleJson userJson);

    @GetMapping(value = FeignConstants.RESOURCE_ROLES + "/{id}")
    CacheResponse<RoleJson> deleteById(@PathVariable(value = "id") final Integer id);
}
