package com.demo.app.feing.cache;

import com.demo.app.controller.json.UserJson;
import com.demo.app.feing.cache.constants.FeignConstants;
import com.demo.app.feing.cache.rest.CacheResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "cache-api", url = "${cache.server.url}")
public interface UserCacheServiceFeignClient {

    @GetMapping(value = FeignConstants.RESOURCE_USERS, produces = MediaType.APPLICATION_JSON_VALUE)
    CacheResponse<UserJson[]> findAllUsers();

    @GetMapping(value = FeignConstants.RESOURCE_USERS + "/{id}")
    CacheResponse<UserJson> findById(@PathVariable(value = "id") final Integer id);

    @GetMapping(value = FeignConstants.RESOURCE_USERS + "/name/{name}")
    CacheResponse<UserJson> findByName(@PathVariable(value = "name") final String name);

    @GetMapping(value = FeignConstants.RESOURCE_USERS + "/username/{username}")
    CacheResponse<UserJson> findByUsername(@PathVariable(value = "username") final String username);

    @PostMapping(value = FeignConstants.RESOURCE_USERS, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    CacheResponse<UserJson> addUser(@RequestBody UserJson userJson);

}
