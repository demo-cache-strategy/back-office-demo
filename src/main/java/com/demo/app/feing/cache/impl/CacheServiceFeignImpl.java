package com.demo.app.feing.cache.impl;

import com.demo.app.controller.json.RoleJson;
import com.demo.app.controller.json.UserJson;
import com.demo.app.exception.NotFoundException;
import com.demo.app.feing.cache.CacheServiceFeign;
import com.demo.app.feing.cache.RoleCacheServiceFeignClient;
import com.demo.app.feing.cache.UserCacheServiceFeignClient;
import com.demo.app.feing.cache.mapper.RoleCacheFeignRestMapper;
import com.demo.app.feing.cache.mapper.UserCacheFeignRestMapper;
import com.demo.app.feing.cache.rest.CacheResponse;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import com.demo.app.util.CommonConstants;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CacheServiceFeignImpl implements CacheServiceFeign {

    private final UserCacheServiceFeignClient userCacheServiceFeignClient;
    private final RoleCacheServiceFeignClient roleCacheServiceFeignClient;
    private final UserCacheFeignRestMapper userCacheFeignRestMapper;
    private final RoleCacheFeignRestMapper roleCacheFeignRestMapper;

    @Override
    public List<UserDto> getAllUsers() {

        return Arrays.stream(userCacheServiceFeignClient.findAllUsers().getData())
                .map(userCacheFeignRestMapper::mapToDto).collect(Collectors.toList());

    }

    @Override
    public UserDto getUserById(UserDto userDto) {
        UserJson userJsonResponse;
        try {
            UserJson userJson = userCacheFeignRestMapper.mapToRest(userDto);
            CacheResponse<UserJson> cacheResponse = userCacheServiceFeignClient.findById(userDto.getId());
            userJsonResponse = cacheResponse.getData();
        } catch (FeignException e) {
            throw new NotFoundException("404", CommonConstants.USER_NOT_FOUND);
        }
        return userCacheFeignRestMapper.mapToDto(userJsonResponse);
    }

    @Override
    public UserDto getUserByName(UserDto userDto) {
        UserJson userJsonResponse;
        try {
        UserJson userJson = userCacheFeignRestMapper.mapToRest(userDto);
        CacheResponse<UserJson> cacheResponse = userCacheServiceFeignClient.findByName(userJson.getName());
        userJsonResponse = cacheResponse.getData();
        } catch (FeignException e) {
            throw new NotFoundException("404", CommonConstants.USER_NOT_FOUND);
        }
        return userCacheFeignRestMapper.mapToDto(userJsonResponse);
    }

    @Override
    public UserDto getUserByUsername(UserDto userDto) {
        UserJson userJsonResponse;
        try {
        UserJson userJson = userCacheFeignRestMapper.mapToRest(userDto);
        CacheResponse<UserJson> cacheResponse = userCacheServiceFeignClient.findByUsername(userJson.getUsername());
        userJsonResponse = cacheResponse.getData();
        } catch (FeignException e) {
            throw new NotFoundException("404", CommonConstants.USER_NOT_FOUND);
        }
        return userCacheFeignRestMapper.mapToDto(userJsonResponse);
    }

    @Override
    public UserDto addUser(UserDto userDto) {
        UserJson userJson = userCacheFeignRestMapper.mapToRest(userDto);
        CacheResponse<UserJson> cacheResponse = userCacheServiceFeignClient.addUser(userJson);
        UserJson userJsonResponse = cacheResponse.getData();
        return userCacheFeignRestMapper.mapToDto(userJsonResponse);
    }

    @Override
    public List<UserDto> addUser(List<UserDto> userDtoList) {
        List<UserDto> userDtoListResponse = new ArrayList<>();
        userDtoList.stream().forEach(userDto -> {
            UserJson userJson = userCacheFeignRestMapper.mapToRest(userDto);
            CacheResponse<UserJson> cacheResponse = userCacheServiceFeignClient.addUser(userJson);
            UserJson userJsonResponse = cacheResponse.getData();
            userDtoListResponse.add(userCacheFeignRestMapper.mapToDto(userJsonResponse));
        });
        return userDtoListResponse;
    }

    @Override
    public List<RoleDto> getAllRoles() {

        return Arrays.stream(roleCacheServiceFeignClient.findAllRoles().getData())
                .map(roleCacheFeignRestMapper::mapToDto).collect(Collectors.toList());

    }

    @Override
    public RoleDto getRoleById(RoleDto roleDto) {
        RoleJson roleJsonResponse;
        try {
            RoleJson roleJson = roleCacheFeignRestMapper.mapToRest(roleDto);
            CacheResponse<RoleJson> cacheResponse = roleCacheServiceFeignClient.findById(roleDto.getId());
            roleJsonResponse = cacheResponse.getData();
        } catch (FeignException e) {
            throw new NotFoundException("404", CommonConstants.USER_NOT_FOUND);
        }
        return roleCacheFeignRestMapper.mapToDto(roleJsonResponse);
    }

    @Override
    public RoleDto getRoleByName(RoleDto roleDto) {
        RoleJson roleJsonResponse;
        try {
            RoleJson roleJson = roleCacheFeignRestMapper.mapToRest(roleDto);
            CacheResponse<RoleJson> cacheResponse = roleCacheServiceFeignClient.findByName(roleJson.getName());
            roleJsonResponse = cacheResponse.getData();
        } catch (FeignException e) {
            throw new NotFoundException("404", CommonConstants.USER_NOT_FOUND);
        }
        return roleCacheFeignRestMapper.mapToDto(roleJsonResponse);
    }

    @Override
    public RoleDto addRole(RoleDto roleDto) {
        RoleJson roleJson = roleCacheFeignRestMapper.mapToRest(roleDto);
        CacheResponse<RoleJson> cacheResponse = roleCacheServiceFeignClient.addRole(roleJson);
        RoleJson roleJsonResponse = cacheResponse.getData();
        return roleCacheFeignRestMapper.mapToDto(roleJsonResponse);
    }

    @Override
    public List<RoleDto> addRole(List<RoleDto> roleDtoList) {
        List<RoleDto> roleDtoListResponse = new ArrayList<>();
        roleDtoList.stream().forEach(roleDto -> {
            RoleJson roleJson = roleCacheFeignRestMapper.mapToRest(roleDto);
            CacheResponse<RoleJson> cacheResponse = roleCacheServiceFeignClient.addRole(roleJson);
            RoleJson roleJsonResponse = cacheResponse.getData();
            roleDtoListResponse.add(roleCacheFeignRestMapper.mapToDto(roleJsonResponse));
        });
        return roleDtoListResponse;
    }

    @Override
    public RoleDto deleteRole(RoleDto roleDto) {
        RoleJson roleJson = roleCacheFeignRestMapper.mapToRest(roleDto);
        CacheResponse<RoleJson> cacheResponse = roleCacheServiceFeignClient.deleteById(roleJson.getId());
        RoleJson roleJsonResponse = cacheResponse.getData();
        return roleCacheFeignRestMapper.mapToDto(roleJsonResponse);
    }
}
