package com.demo.app.feing.cache.mapper;

import com.demo.app.controller.json.UserJson;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class UserCacheFeignRestMapper {

    public UserJson mapToRest(UserDto dto) {
        if (null == dto) {
            return null;
        }

        final UserJson json = new UserJson();

        json.setId(dto.getId());
        json.setName(dto.getName());
        json.setUsername(dto.getUsername());
        json.setPassword(dto.getPassword());
        json.setMail(dto.getMail());
        json.setStatus(dto.getStatus());
        if (null != dto.getRoles()) {
            json.setRoles(dto.getRoles().stream().map(RoleDto::getId).collect(Collectors.toList()));
        }

        return json;
    }

    public UserDto mapToDto(UserJson json) {
        if (null == json) {
            return null;
        }

        final UserDto dto = new UserDto();

        dto.setId(json.getId());
        dto.setName(json.getName());
        dto.setUsername(json.getUsername());
        dto.setPassword(json.getPassword());
        dto.setMail(json.getMail());
        dto.setStatus(json.getStatus());
        if (null != json.getRoles() && !json.getRoles().isEmpty()) {
            dto.setRoles(json.getRoles().stream().map(roleId -> RoleDto.builder().id(roleId).build()).collect(Collectors.toList()));
        }

        return dto;
    }

    public List<UserJson> mapToJsonList(final Collection<UserDto> dtoList) {
        return dtoList.stream().map(this::mapToRest).collect(Collectors.toList());
    }

    public List<UserDto> mapToDtoList(final Collection<UserJson> jsonList) {
        return jsonList.stream().map(this::mapToDto).collect(Collectors.toList());
    }
}
