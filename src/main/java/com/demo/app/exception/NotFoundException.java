package com.demo.app.exception;

import org.springframework.http.HttpStatus;

import com.demo.app.exception.error.ErrorCollection;

public class NotFoundException extends DemoException {

	private static final long serialVersionUID = 1147677766584826705L;

	public NotFoundException(final String code, final String message) {
		super(code, HttpStatus.NOT_FOUND.value(), message);
	}

	public NotFoundException(final String code, final String message, final ErrorCollection data) {
		super(code, HttpStatus.NOT_FOUND.value(), message, data);
	}
}
