package com.demo.app.exception;

import com.demo.app.exception.error.ErrorCollection;

import lombok.Getter;

@Getter
public class DemoException extends RuntimeException {

	private static final long serialVersionUID = 2511898525250120134L;

	private final String code;

	private final int responseCode;

	private final ErrorCollection errorList;

	public DemoException(final String code, final int responseCode, final String message) {
		this(code, responseCode, message, null);
	}

	public DemoException(final String code, final int responseCode, final String message,
                         final ErrorCollection errorList) {
		super(message);
		this.code = code;
		this.responseCode = responseCode;
		this.errorList = errorList;
	}

}
