package com.demo.app.exception.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Error implements Serializable {

	private static final long serialVersionUID = -8502900941111111784L;

	private String name;

	private String value;

}
