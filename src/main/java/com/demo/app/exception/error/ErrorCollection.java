package com.demo.app.exception.error;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Getter
@NoArgsConstructor
public class ErrorCollection implements Serializable {

	private static final long serialVersionUID = -511214055167796590L;

	private final Collection<Error> errors = new ArrayList<>();

	public ErrorCollection(final Error error) {
		super();
		addError(error);
	}

	public ErrorCollection(final Collection<Error> errors) {
		super();
		addErrors(errors);
	}

	public void addError(final Error error) {
		this.errors.add(error);
	}

	public void addErrors(final Collection<Error> errors) {
		this.errors.addAll(errors);
	}

}
