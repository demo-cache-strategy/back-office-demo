package com.demo.app.exception;

import org.springframework.http.HttpStatus;

import com.demo.app.exception.error.ErrorCollection;
import com.demo.app.util.ExceptionConstants;

public class BadRequestException extends DemoException {

	private static final long serialVersionUID = -4678495481421270851L;

	public BadRequestException(final String message) {
		super(ExceptionConstants.CODE_BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), message);
	}

	public BadRequestException(final String code, final String message) {
		super(code, HttpStatus.BAD_REQUEST.value(), message);
	}

	public BadRequestException(final String code, final String message, final ErrorCollection data) {
		super(code, HttpStatus.BAD_REQUEST.value(), message, data);
	}

}
