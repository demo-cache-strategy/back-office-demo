package com.demo.app.exception;

import org.springframework.http.HttpStatus;

public class InternalServerErrorException extends DemoException {

	private static final long serialVersionUID = -4985693228110224616L;

	public InternalServerErrorException(final String code, final String message) {
		super(code, HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
	}

}
