package com.demo.app.service;

import com.demo.app.exception.BadRequestException;
import com.demo.app.exception.NotFoundException;
import com.demo.app.repository.RoleRepository;
import com.demo.app.repository.UserRepository;
import com.demo.app.repository.entity.Role;
import com.demo.app.repository.entity.User;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.impl.RoleServiceImpl;
import com.demo.app.service.mapper.ServiceMapper;
import com.demo.app.service.mapper.impl.RoleServiceMapper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceTest {
//	private static final Integer ROLE_ID_PARAM = 1;
//	private static final String ROLE_VALUE_PARAM = "ROOT";
//	private static final Integer BAD_ROLE_ID_PARAM = 0;
//	private static final String BAD_ROLE_VALUE_PARAM = "WRONG";
//	private static final Integer USER_ID_PARAM = 1;
//	private static final String USER_NAME_PARAM = "Test Name";
//	private static final String USER_USERNAME_PARAM = "test";
//	private static final String USER_PASSWORD_PARAM = "123";
//	private static final String USER_MAIL_PARAM = "test@test.com";
//	private static final ServiceMapper<RoleDto, Role> roleServiceMapper = new RoleServiceMapper();
//
//	@Mock
//	private RoleRepository roleRepository;
//
//	@Mock
//	private UserRepository userRepository;
//
//	@Mock
//	private final ServiceMapper<RoleDto, Role> serviceMapper = new RoleServiceMapper();
//
//	@InjectMocks
//	private final RoleService roleService = new RoleServiceImpl(serviceMapper, );
//
//	@Rule
//	public ExpectedException expectedException = ExpectedException.none();
//
//	@Test
//	public void should_return_a_role_when_we_find_by_id() {
//		// Given
//		final Role roleMocked;
//		roleMocked = new Role();
//		roleMocked.setId(ROLE_ID_PARAM);
//		roleMocked.setName(ROLE_VALUE_PARAM);
//		roleMocked.setUsers(new HashSet<User>());
//
//		final User userMocked;
//		userMocked = new User();
//		userMocked.setId(USER_ID_PARAM);
//		userMocked.setName(USER_NAME_PARAM);
//		userMocked.setUsername(USER_USERNAME_PARAM);
//		userMocked.setPassword(USER_PASSWORD_PARAM);
//		userMocked.setMail(USER_MAIL_PARAM);
//
//		roleMocked.getUsers().add(userMocked);
//
//		final Optional<Role> optionalRole = Optional.of(roleMocked);
//		final RoleDto roleDtoRequest = roleServiceMapper.mapToDto(roleMocked);
//		when(roleRepository.findById(ROLE_ID_PARAM)).thenReturn(optionalRole);
//		when(serviceMapper.mapToDto(roleMocked)).thenReturn(roleDtoRequest);
//
//		// When
//		final RoleDto roleDtoResponse = roleService.findById(ROLE_ID_PARAM);
//
//		// Then
//		assertThat(roleDtoRequest.equals(roleDtoResponse)).isTrue();
//	}
//
//	@Test
//	public void should_return_a_role_when_we_find_by_name() {
//		// Given
//		final Role roleMocked;
//		roleMocked = new Role();
//		roleMocked.setId(ROLE_ID_PARAM);
//		roleMocked.setName(ROLE_VALUE_PARAM);
//		roleMocked.setUsers(new HashSet<User>());
//
//		final User userMocked;
//		userMocked = new User();
//		userMocked.setId(USER_ID_PARAM);
//		userMocked.setName(USER_NAME_PARAM);
//		userMocked.setUsername(USER_USERNAME_PARAM);
//		userMocked.setPassword(USER_PASSWORD_PARAM);
//		userMocked.setMail(USER_MAIL_PARAM);
//
//		roleMocked.getUsers().add(userMocked);
//
//		final Optional<Role> optionalRole = Optional.of(roleMocked);
//		final RoleDto roleDtoRequest = roleServiceMapper.mapToDto(roleMocked);
//		when(roleRepository.findByName(ROLE_VALUE_PARAM)).thenReturn(optionalRole);
//		when(serviceMapper.mapToDto(roleMocked)).thenReturn(roleDtoRequest);
//
//		// When
//		final RoleDto roleDtoResponse = roleService.findByName(ROLE_VALUE_PARAM);
//
//		// Then
//		assertThat(roleDtoRequest.equals(roleDtoResponse)).isTrue();
//	}
//
//	@Test
//	public void should_throw_NotFoundException_when_role_not_exist_by_id() {
//		// Given
//		final Role roleMocked;
//		roleMocked = new Role();
//		roleMocked.setId(BAD_ROLE_ID_PARAM);
//		roleMocked.setName(ROLE_VALUE_PARAM);
//		roleMocked.setUsers(new HashSet<User>());
//
//		final User userMocked;
//		userMocked = new User();
//		userMocked.setId(USER_ID_PARAM);
//		userMocked.setName(USER_NAME_PARAM);
//		userMocked.setUsername(USER_USERNAME_PARAM);
//		userMocked.setPassword(USER_PASSWORD_PARAM);
//		userMocked.setMail(USER_MAIL_PARAM);
//
//		roleMocked.getUsers().add(userMocked);
//
//		final RoleDto roleDtoRequest = roleServiceMapper.mapToDto(roleMocked);
//		when(roleRepository.findById(BAD_ROLE_ID_PARAM)).thenThrow(NotFoundException.class);
//		expectedException.expect(NotFoundException.class);
//
//		// When
//		roleService.findById(roleDtoRequest.getId());
//	}
//
//	@Test
//	public void should_throw_NotFoundException_when_role_not_exist_by_name() {
//		// Given
//		final Role roleMocked;
//		roleMocked = new Role();
//		roleMocked.setId(ROLE_ID_PARAM);
//		roleMocked.setName(BAD_ROLE_VALUE_PARAM);
//		roleMocked.setUsers(new HashSet<User>());
//
//		final User userMocked;
//		userMocked = new User();
//		userMocked.setId(USER_ID_PARAM);
//		userMocked.setName(USER_NAME_PARAM);
//		userMocked.setUsername(USER_USERNAME_PARAM);
//		userMocked.setPassword(USER_PASSWORD_PARAM);
//		userMocked.setMail(USER_MAIL_PARAM);
//
//		roleMocked.getUsers().add(userMocked);
//
//		final RoleDto roleDtoRequest = roleServiceMapper.mapToDto(roleMocked);
//		when(roleRepository.findByName(BAD_ROLE_VALUE_PARAM)).thenThrow(NotFoundException.class);
//		expectedException.expect(NotFoundException.class);
//
//		// When
//		roleService.findByName(roleDtoRequest.getName());
//	}
//
//	@Test
//	public void should_throw_BadRequestException_when_we_find_by_id_and_param_is_null() {
//		// Given
//		expectedException.expect(BadRequestException.class);
//
//		// When
//		roleService.findById(null);
//	}
//
//	@Test
//	public void should_throw_BadRequestException_when_we_find_by_name_and_param_is_null() {
//		// Given
//		expectedException.expect(BadRequestException.class);
//
//		// When
//		roleService.findByName(null);
//	}
//
//	@Test
//	public void should_throw_BadRequestException_when_we_save_a_role_and_param_is_null() {
//		// Given
//		expectedException.expect(BadRequestException.class);
//
//		// When
//		roleService.save(null);
//	}
//
//	@Test
//	public void should_throw_BadRequestException_when_we_update_a_role_and_param_is_null() {
//		// Given
//		expectedException.expect(BadRequestException.class);
//
//		// When
//		roleService.update(null);
//	}
//
//	@Test
//	public void should_throw_BadRequestException_when_we_delete_a_role_and_param_is_null() {
//		// Given
//		expectedException.expect(BadRequestException.class);
//
//		// When
//		roleService.delete(null);
//	}
//
//	@Test
//	public void should_throw_BadRequestException_when_we_update_a_role_and_param_id_is_null() {
//		// Given
//		final Role roleMocked;
//		roleMocked = new Role();
//		roleMocked.setId(null);
//		roleMocked.setName(ROLE_VALUE_PARAM);
//		roleMocked.setUsers(new HashSet<User>());
//
//		final User userMocked;
//		userMocked = new User();
//		userMocked.setId(USER_ID_PARAM);
//		userMocked.setName(USER_NAME_PARAM);
//		userMocked.setUsername(USER_USERNAME_PARAM);
//		userMocked.setPassword(USER_PASSWORD_PARAM);
//		userMocked.setMail(USER_MAIL_PARAM);
//
//		roleMocked.getUsers().add(userMocked);
//
//		final RoleDto roleDtoRequest = roleServiceMapper.mapToDto(roleMocked);
//
//		expectedException.expect(BadRequestException.class);
//
//		// When
//		roleService.update(roleDtoRequest);
//	}
//
//	@Test
//	public void should_throw_BadRequestException_when_we_delete_a_role_and_param_id_is_null() {
//		// Given
//		final Role roleMocked;
//		roleMocked = new Role();
//		roleMocked.setId(null);
//		roleMocked.setName(ROLE_VALUE_PARAM);
//		roleMocked.setUsers(new HashSet<User>());
//
//		final User userMocked;
//		userMocked = new User();
//		userMocked.setId(USER_ID_PARAM);
//		userMocked.setName(USER_NAME_PARAM);
//		userMocked.setUsername(USER_USERNAME_PARAM);
//		userMocked.setPassword(USER_PASSWORD_PARAM);
//		userMocked.setMail(USER_MAIL_PARAM);
//
//		roleMocked.getUsers().add(userMocked);
//
//		final RoleDto roleDtoRequest = roleServiceMapper.mapToDto(roleMocked);
//		expectedException.expect(BadRequestException.class);
//
//		// When
//		roleService.delete(roleDtoRequest);
//	}
}
