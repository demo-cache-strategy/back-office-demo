package com.demo.app.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.demo.app.repository.entity.Role;
import com.demo.app.repository.entity.User;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import com.demo.app.service.mapper.impl.UserServiceMapper;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceMapperTest {

	private static final Integer USER_ID = 1;
	private static final String USER_NAME = "test";
	private static final String USER_USERNAME = "test";
	private static final String USER_PASSWORD = "123";
	private static final String USER_MAIL = "test@test.com";

	private static final Integer ROLE_ID = 1;
	private static final String ROLE_NAME = "ROOT";

	@InjectMocks
	private final ServiceMapper<UserDto, User> userServiceMapper = new UserServiceMapper();

	@Test
	public void should_return_a_user_entity_with_all_fields_filled_ok() {
		// Given
		final UserDto userDto = new UserDto();
		final RoleDto roleDto = new RoleDto();
		final User userForTest;
		final Role roleForTest;
		final User userResult;

		userDto.setId(USER_ID);
		userDto.setName(USER_NAME);
		userDto.setUsername(USER_USERNAME);
		userDto.setPassword(USER_PASSWORD);
		userDto.setMail(USER_MAIL);
		userDto.setRoles(new ArrayList<>());

		roleDto.setId(ROLE_ID);
		roleDto.setName(ROLE_NAME);

		userDto.getRoles().add(roleDto);

		userForTest = new User();
		userForTest.setId(USER_ID);
		userForTest.setName(USER_NAME);
		userForTest.setUsername(USER_USERNAME);
		userForTest.setPassword(USER_PASSWORD);
		userForTest.setMail(USER_MAIL);
		userForTest.setRoles(new HashSet<>());

		roleForTest = new Role();
		roleForTest.setId(ROLE_ID);
		roleForTest.setName(ROLE_NAME);

		userForTest.getRoles().add(roleForTest);

		// when
		userResult = userServiceMapper.mapToEntity(userDto);

		// Then
		assertThat(userResult.getId()).isEqualTo(userForTest.getId());
		assertThat(userResult.getMail()).isEqualTo(userForTest.getMail());
		assertThat(userResult.getName()).isEqualTo(userForTest.getName());
		assertThat(userResult.getPassword()).isEqualTo(userForTest.getPassword());
		assertThat(userResult.getStatus()).isEqualTo(userForTest.getStatus());
		assertThat(userResult.getUsername()).isEqualTo(userForTest.getUsername());

		assertThat(userResult.getRoles().stream().collect(Collectors.toList()).get(0).getId())
				.isEqualTo(userForTest.getRoles().stream().collect(Collectors.toList()).get(0).getId());
		assertThat(userResult.getRoles().stream().collect(Collectors.toList()).get(0).getName())
				.isEqualTo(userForTest.getRoles().stream().collect(Collectors.toList()).get(0).getName());
		assertThat(userResult.getRoles().stream().collect(Collectors.toList()).get(0).getUsers())
				.isEqualTo(userForTest.getRoles().stream().collect(Collectors.toList()).get(0).getUsers());
	}

	@Test
	public void should_return_a_user_dto_with_all_fields_filled_ok() {
		// Given
		final User user = new User();
		final Role role = new Role();
		final UserDto userDtoForTest;
		final RoleDto roleDtoForTest;
		final UserDto userDtoResult;

		user.setId(USER_ID);
		user.setName(USER_NAME);
		user.setUsername(USER_USERNAME);
		user.setPassword(USER_PASSWORD);
		user.setMail(USER_MAIL);
		user.setRoles(new HashSet<Role>());

		role.setId(ROLE_ID);
		role.setName(ROLE_NAME);

		user.getRoles().add(role);

		userDtoForTest = new UserDto();
		userDtoForTest.setId(USER_ID);
		userDtoForTest.setName(USER_NAME);
		userDtoForTest.setUsername(USER_USERNAME);
		userDtoForTest.setPassword(USER_PASSWORD);
		userDtoForTest.setMail(USER_MAIL);
		userDtoForTest.setRoles(new ArrayList<>());

		roleDtoForTest = new RoleDto();
		roleDtoForTest.setId(ROLE_ID);
		roleDtoForTest.setName(ROLE_NAME);

		userDtoForTest.getRoles().add(roleDtoForTest);

		// when
		userDtoResult = userServiceMapper.mapToDto(user);

		// Then
		assertThat(userDtoResult).isEqualTo(userDtoForTest);
	}
}
