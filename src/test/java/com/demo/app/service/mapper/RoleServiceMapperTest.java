package com.demo.app.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.demo.app.repository.entity.Role;
import com.demo.app.repository.entity.User;
import com.demo.app.service.dto.RoleDto;
import com.demo.app.service.dto.UserDto;
import com.demo.app.service.mapper.impl.RoleServiceMapper;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceMapperTest {

	private static final Integer ROLE_ID = 1;
	private static final String ROLE_NAME = "ROOT";

	private static final Integer USER_ID = 1;
	private static final String USER_NAME = "test";
	private static final String USER_USERNAME = "test";
	private static final String USER_PASSWORD = "123";
	private static final String USER_MAIL = "test@test.com";

	@InjectMocks
	private final ServiceMapper<RoleDto, Role> roleServiceMapper = new RoleServiceMapper();

	@Test
	public void should_return_a_role_entity_with_all_fields_filled_ok() {
		// Given
		final RoleDto roleDto = new RoleDto();
		final UserDto userDto = new UserDto();
		final Role roleForTest;
		final User userForTest;
		final Role roleResult;

		roleDto.setId(ROLE_ID);
		roleDto.setName(ROLE_NAME);
		roleDto.setUsers(new ArrayList<>());

		userDto.setId(USER_ID);
		userDto.setName(USER_NAME);
		userDto.setUsername(USER_USERNAME);
		userDto.setPassword(USER_PASSWORD);
		userDto.setMail(USER_MAIL);

		roleDto.getUsers().add(userDto);

		roleForTest = new Role();
		roleForTest.setId(ROLE_ID);
		roleForTest.setName(ROLE_NAME);
		roleForTest.setUsers(new HashSet<>());

		userForTest = new User();
		userForTest.setId(USER_ID);
		userForTest.setName(USER_NAME);
		userForTest.setUsername(USER_USERNAME);
		userForTest.setPassword(USER_PASSWORD);
		userForTest.setMail(USER_MAIL);

		roleForTest.getUsers().add(userForTest);

		// when
		roleResult = roleServiceMapper.mapToEntity(roleDto);

		// Then
		assertThat(roleResult.getId()).isEqualTo(roleForTest.getId());
		assertThat(roleResult.getName()).isEqualTo(roleForTest.getName());
		assertThat(roleResult.getUsers().stream().collect(Collectors.toList()).get(0).getId())
				.isEqualTo(roleForTest.getUsers().stream().collect(Collectors.toList()).get(0).getId());
		assertThat(roleResult.getUsers().stream().collect(Collectors.toList()).get(0).getMail())
				.isEqualTo(roleForTest.getUsers().stream().collect(Collectors.toList()).get(0).getMail());
		assertThat(roleResult.getUsers().stream().collect(Collectors.toList()).get(0).getName())
				.isEqualTo(roleForTest.getUsers().stream().collect(Collectors.toList()).get(0).getName());
		assertThat(roleResult.getUsers().stream().collect(Collectors.toList()).get(0).getPassword())
				.isEqualTo(roleForTest.getUsers().stream().collect(Collectors.toList()).get(0).getPassword());
		assertThat(roleResult.getUsers().stream().collect(Collectors.toList()).get(0).getRoles())
				.isEqualTo(roleForTest.getUsers().stream().collect(Collectors.toList()).get(0).getRoles());
		assertThat(roleResult.getUsers().stream().collect(Collectors.toList()).get(0).getStatus())
				.isEqualTo(roleForTest.getUsers().stream().collect(Collectors.toList()).get(0).getStatus());
		assertThat(roleResult.getUsers().stream().collect(Collectors.toList()).get(0).getUsername())
				.isEqualTo(roleForTest.getUsers().stream().collect(Collectors.toList()).get(0).getUsername());
	}

	@Test
	public void should_return_a_role_dto_with_all_fields_filled_ok() {
		// Given
		final Role role = new Role();
		final User user = new User();
		final RoleDto roleDtoForTest;
		final UserDto userDtoForTest;
		final RoleDto roleDtoResult;

		role.setId(ROLE_ID);
		role.setName(ROLE_NAME);
		role.setUsers(new HashSet<>());

		user.setId(USER_ID);
		user.setName(USER_NAME);
		user.setUsername(USER_USERNAME);
		user.setPassword(USER_PASSWORD);
		user.setMail(USER_MAIL);

		role.getUsers().add(user);

		roleDtoForTest = new RoleDto();
		roleDtoForTest.setId(ROLE_ID);
		roleDtoForTest.setName(ROLE_NAME);
		roleDtoForTest.setUsers(new ArrayList<>());

		userDtoForTest = new UserDto();
		userDtoForTest.setId(USER_ID);
		userDtoForTest.setName(USER_NAME);
		userDtoForTest.setUsername(USER_USERNAME);
		userDtoForTest.setPassword(USER_PASSWORD);
		userDtoForTest.setMail(USER_MAIL);

		roleDtoForTest.getUsers().add(userDtoForTest);

		// when
		roleDtoResult = roleServiceMapper.mapToDto(role);

		// Then
		assertThat(roleDtoResult).isEqualTo(roleDtoForTest);
	}
}
