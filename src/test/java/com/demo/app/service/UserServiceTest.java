package com.demo.app.service;

import com.demo.app.exception.BadRequestException;
import com.demo.app.exception.NotFoundException;
import com.demo.app.repository.UserRepository;
import com.demo.app.repository.entity.Role;
import com.demo.app.repository.entity.User;
import com.demo.app.service.dto.UserDto;
import com.demo.app.service.impl.UserServiceImpl;
import com.demo.app.service.mapper.ServiceMapper;
import com.demo.app.service.mapper.impl.UserServiceMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	private static final Integer USER_ID_PARAM = 1;
	private static final String USER_NAME_PARAM = "Test Name";
	private static final String USER_USERNAME_PARAM = "test";
	private static final String USER_PASSWORD_PARAM = "123";
	private static final String USER_MAIL_PARAM = "test@test.com";
	private static final Integer ROLE_ID_PARAM = 1;
	private static final String ROLE_VALUE_PARAM = "ROOT";
	private static final Integer BAD_USER_ID_PARAM = 0;
	private static final String BAD_USER_NAME_PARAM = "Test Name Bad";
	private static final ServiceMapper<UserDto, User> userServiceMapper = new UserServiceMapper();

	@Mock
	private UserRepository userRepository;

	@Mock
	private final ServiceMapper<UserDto, User> serviceMapper = new UserServiceMapper();

	@InjectMocks
	private UserServiceImpl userServiceImpl;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Before
	public void init(){
		userServiceImpl  = new UserServiceImpl(userRepository, userServiceMapper, null, null);
	}

	@Test
	public void should_return_a_user_when_we_find_by_id() {
		// Given
		final User userMocked;
		userMocked = new User();
		userMocked.setId(USER_ID_PARAM);
		userMocked.setName(USER_NAME_PARAM);
		userMocked.setUsername(USER_USERNAME_PARAM);
		userMocked.setPassword(USER_PASSWORD_PARAM);
		userMocked.setMail(USER_MAIL_PARAM);
		userMocked.setRoles(new HashSet<>());

		final Role roleMocked;
		roleMocked = new Role();
		roleMocked.setId(ROLE_ID_PARAM);
		roleMocked.setName(ROLE_VALUE_PARAM);

		userMocked.getRoles().add(roleMocked);

		final Optional<User> optionalUser = Optional.of(userMocked);
		final UserDto userDtoRequest = userServiceMapper.mapToDto(userMocked);
		when(userRepository.findById(USER_ID_PARAM)).thenReturn(optionalUser);

		// When
		final UserDto userDtoResponse = userServiceImpl.findById(USER_ID_PARAM);

		// Then
		assertThat(userDtoRequest.equals(userDtoResponse)).isTrue();
	}

	@Test
	public void should_return_a_user_when_we_find_by_name() {
		// Given
		final User userMocked;
		userMocked = new User();
		userMocked.setId(USER_ID_PARAM);
		userMocked.setName(USER_NAME_PARAM);
		userMocked.setUsername(USER_USERNAME_PARAM);
		userMocked.setPassword(USER_PASSWORD_PARAM);
		userMocked.setMail(USER_MAIL_PARAM);
		userMocked.setRoles(new HashSet<>());

		final Role roleMocked;
		roleMocked = new Role();
		roleMocked.setId(ROLE_ID_PARAM);
		roleMocked.setName(ROLE_VALUE_PARAM);

		userMocked.getRoles().add(roleMocked);
		final Optional<User> optionalUser = Optional.of(userMocked);
		final UserDto userDtoRequest = userServiceMapper.mapToDto(userMocked);
		doReturn(optionalUser).when(userRepository).findByName(USER_NAME_PARAM);

		// When
		final UserDto userDtoResponse = userServiceImpl.findByName(USER_NAME_PARAM);

		// Then
		assertThat(userDtoRequest.equals(userDtoResponse)).isTrue();
	}

	@Test
	public void should_throw_NotFoundException_when_user_not_exist_by_id() {
		// Given
		final User userMocked;
		userMocked = new User();
		userMocked.setId(BAD_USER_ID_PARAM);
		userMocked.setName(USER_NAME_PARAM);
		userMocked.setUsername(USER_USERNAME_PARAM);
		userMocked.setPassword(USER_PASSWORD_PARAM);
		userMocked.setMail(USER_MAIL_PARAM);
		userMocked.setRoles(new HashSet<>());

		final Role roleMocked;
		roleMocked = new Role();
		roleMocked.setId(ROLE_ID_PARAM);
		roleMocked.setName(ROLE_VALUE_PARAM);

		userMocked.getRoles().add(roleMocked);

		final UserDto userDtoRequest = userServiceMapper.mapToDto(userMocked);
		when(userRepository.findById(BAD_USER_ID_PARAM)).thenThrow(NotFoundException.class);
		expectedException.expect(NotFoundException.class);

		// When
		userServiceImpl.findById(userDtoRequest.getId());
	}

	@Test
	public void should_throw_NotFoundException_when_user_not_exist_by_name() {
		// Given
		final User userMocked;
		userMocked = new User();
		userMocked.setId(USER_ID_PARAM);
		userMocked.setName(BAD_USER_NAME_PARAM);
		userMocked.setUsername(USER_USERNAME_PARAM);
		userMocked.setPassword(USER_PASSWORD_PARAM);
		userMocked.setMail(USER_MAIL_PARAM);
		userMocked.setRoles(new HashSet<>());

		final Role roleMocked;
		roleMocked = new Role();
		roleMocked.setId(ROLE_ID_PARAM);
		roleMocked.setName(ROLE_VALUE_PARAM);

		userMocked.getRoles().add(roleMocked);

		final UserDto userDtoRequest = userServiceMapper.mapToDto(userMocked);
		when(userRepository.findByName(BAD_USER_NAME_PARAM)).thenThrow(NotFoundException.class);
		expectedException.expect(NotFoundException.class);

		// When
		userServiceImpl.findByName(userDtoRequest.getName());
	}

	@Test
	public void should_throw_BadRequestException_when_we_find_by_id_and_param_is_null() {
		// Given
		expectedException.expect(BadRequestException.class);

		// When
		userServiceImpl.findById(null);
	}

	@Test
	public void should_throw_BadRequestException_when_we_find_by_name_and_param_is_null() {
		// Given
		expectedException.expect(BadRequestException.class);

		// When
		userServiceImpl.findByName(null);
	}

	@Test
	public void should_throw_BadRequestException_when_we_save_a_user_and_param_is_null() {
		// Given
		expectedException.expect(BadRequestException.class);

		// When
		userServiceImpl.save(null);
	}

	@Test
	public void should_throw_BadRequestException_when_we_update_a_user_and_param_is_null() {
		// Given
		expectedException.expect(BadRequestException.class);

		// When
		userServiceImpl.update(null);
	}

	@Test
	public void should_throw_BadRequestException_when_we_delete_a_user_and_param_is_null() {
		// Given
		expectedException.expect(BadRequestException.class);

		// When
		userServiceImpl.delete(null);
	}

	@Test
	public void should_throw_BadRequestException_when_we_update_a_user_and_param_id_is_null() {
		// Given
		final User userMocked;
		userMocked = new User();
		userMocked.setId(null);
		userMocked.setName(USER_NAME_PARAM);
		userMocked.setUsername(USER_USERNAME_PARAM);
		userMocked.setPassword(USER_PASSWORD_PARAM);
		userMocked.setMail(USER_MAIL_PARAM);
		userMocked.setRoles(new HashSet<Role>());

		final Role roleMocked;
		roleMocked = new Role();
		roleMocked.setId(ROLE_ID_PARAM);
		roleMocked.setName(ROLE_VALUE_PARAM);

		userMocked.getRoles().add(roleMocked);
		final UserDto userDtoRequest = userServiceMapper.mapToDto(userMocked);
		expectedException.expect(BadRequestException.class);

		// When
		userServiceImpl.update(userDtoRequest);
	}

	@Test
	public void should_throw_BadRequestException_when_we_delete_a_user_and_param_id_is_null() {
		// Given
		final User userMocked;
		userMocked = new User();
		userMocked.setId(null);
		userMocked.setName(USER_NAME_PARAM);
		userMocked.setUsername(USER_USERNAME_PARAM);
		userMocked.setPassword(USER_PASSWORD_PARAM);
		userMocked.setMail(USER_MAIL_PARAM);
		userMocked.setRoles(new HashSet<Role>());

		final Role roleMocked;
		roleMocked = new Role();
		roleMocked.setId(ROLE_ID_PARAM);
		roleMocked.setName(ROLE_VALUE_PARAM);

		userMocked.getRoles().add(roleMocked);

		final UserDto userDtoRequest = userServiceMapper.mapToDto(userMocked);
		expectedException.expect(BadRequestException.class);

		// When
		userServiceImpl.delete(userDtoRequest);
	}
}
