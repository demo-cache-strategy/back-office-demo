# Redis Cache

Esta PoC es una representacion de 3 tipos de estrategias de cache:
  
*  Cache Aside
*  Read-Through Cache
*  Write-Through Cache

**Para Cache Aside**: Aplicamos esta estrategia la parte que gestiona los usuarios haciendo que la 
app lea primero en el cache los datos y si existen simplemente se retornan, caso
contrario se hace una busqueda en la BD y si se encuentran se almacenan en la
cache y luego se retornan. Para el caso de la escritura de los datos se hace 
directo en la BD.

**Para Read-Through Cache y Write-Through Cache**: Lo que se hizo fue combinar ambas 
estrategias para simular un proceso que gestione el cache y el almacenamiento en
BD para los roles de ususarios tal y como se esperaria en estos casos ya que lo 
mas comun es que exista un servicio o plataforma que realize este trabajo independientemente 
de nuestra app haciendo sea mas sencilla de desarrollar y mantener.
  
La PoC consta de 2 partes:
*  Back office: servicio para gestion basica de usuarios y roles
*  Cache: servicio para la gestion de cache usando REDIS

**Back office**: Es un microservicio que expone los endpoinds que gestionan los usuarios y 
roles (CRUD) apoyandose del microservicio que gestiona el cache. Para esto
usamos la libreria feign que nos provee un medio rapido para la llamada a
servicios externos a nuestra aplicacion.

**Cache**: Es un microservicio que expone los endpoints que gestionan los datos de los 
usuarios y roles dentro del repositorio destinado al almacenamiento de los
mismos. Para lograr esto, en el caso del cache propiamente dicho, usamos una
base datos Redis y para la persistencia fisica usamos MySQL, en ambos gestionamos
las entidades usando Spring Framework (Spring Data Redis y Spring Data JPA).

# Referente al uso de Redis

**Para montar imagen y contenedor REDIS en Docker:**
*  docker run -d -p 6379:6379 redis

**Para iniciar y detener contenedor:**
*  docker start [CONTEINER_ID]
*  docker stop [CONTEINER_ID]

**Para conectarse a la consola del contenedor:**
*  docker container exec -it [CONTEINER_ID] /bin/bash

**Sentencias para REDIS:**
*  Listar todas las keys: KEYS *
*  Borrar todas las keys: FLUSHALL




Fuentes de investigacion:

* **https://codeahoy.com/2017/08/11/caching-strategies-and-how-to-choose-the-right-one/**
* **https://dzone.com/articles/using-read-through-amp-write-through-in-distribute**
* **https://www.baeldung.com/spring-data-redis-tutorial**
* **https://docs.spring.io/spring-data/data-redis/docs/current/reference/html/#why-spring-redis**